import boto3

AWS_REGION = "us-east-1"

ssm_client = boto3.client("ssm", region_name=AWS_REGION)

new_list_parameter = ssm_client.put_parameter(
    Name='/test/ec2Instance/tags',
    Description='EC2 Instance tags for Test environment',
    Value='test,staging,pre-production',
    Type='StringList',
    Overwrite=True,
    Tier='Standard',
    DataType='text'
)

print(
    f"StringList Parameter created with version {new_list_parameter['Version']} and Tier {new_list_parameter['Tier']}"
)
