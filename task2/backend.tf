terraform {
  backend "s3" {
    encrypt = true
    bucket = "finiata-terraform-state-s3"
    region = "us-east-1"
    dynamodb_table = "finiata-terraform-state-lock-dynamo"

  }
}

