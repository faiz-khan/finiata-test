
provider "aws" {
   region  = "us-east-1"
   profile = "finiataprofile"
}

resource "tls_private_key" "webserver_private_key" {
    algorithm   =  "RSA"
    rsa_bits    =  4096
}
resource "local_file" "private_key" {
    content         =  tls_private_key.webserver_private_key.private_key_pem
    filename        =  "webserver_key.pem"
    file_permission =  0400
}
resource "aws_key_pair" "webserver_key" {
    key_name   = "webserver"
    public_key = tls_private_key.webserver_private_key.public_key_openssh
}
resource "aws_security_group" "allow_http_ssh" {
  name        = "allow_http"
  description = "Allow http inbound traffic"
ingress {
    description = "http"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
 
  }
ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   }
ingress {
    description = "ssh"
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    Name = "allow_http_ssh"
  }
}
module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "instance_by_module"

  ami                    = "ami-0e472ba40eb589f49"
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.webserver_key.key_name
  monitoring             = true
  vpc_security_group_ids = ["${aws_security_group.allow_http_ssh.name}"]
   user_data = <<-EOF
        #!/bin/bash
        echo "*** Installing apache2"
        sudo apt update -y
        sudo apt install apache2 -y
        echo "*** Completed Installing apache2"
        sudo sed -i 's/80/8081/' /etc/apache2/sites-enabled/000-default.conf
        sudo sed -i 's/80/8081/' /etc/apache2/ports.conf
        echo "<p> Created for FINIATA task </p>" > /var/www/html/index.html
        service apache2 restart
        sudo apt install python3-httpbin -y
        ./httpbin --port = "8081"
  EOF

  tags = {
    Terraform   = "true"
    Environment = "test"
    Name = "finiata-machine-3"
  }
}

resource "aws_instance" "webserver" {
  count = 2
  ami           = var.ami
  instance_type = var.instance_type 
  key_name  = aws_key_pair.webserver_key.key_name
  security_groups=["${aws_security_group.allow_http_ssh.name}"]
  user_data = <<-EOF
	#!/bin/bash
	echo "*** Installing apache2"
	sudo apt update -y
	sudo apt install apache2 -y
	echo "*** Completed Installing apache2"
	sudo sed -i 's/80/8080/' /etc/apache2/sites-enabled/000-default.conf
	sudo sed -i 's/80/8080/' /etc/apache2/ports.conf
	echo "<p> Created for FINIATA task </p>" > /var/www/html/index.html
	service apache2 restart
	sudo apt install python3-httpbin -y
	./httpbin --port = "8080"
  EOF
tags = {
    Name = "finiata-machine-${count.index}"
  }
}
output "webserver-ip"{
 value=aws_instance.webserver.*.public_ip
}
